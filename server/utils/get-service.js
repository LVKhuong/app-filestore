const lodash = require("lodash");

// Service
async function getService(app, serviceName) {
  const injektor = await app.runner.invoke(lodash.identity);
  const sandboxManager = injektor.lookup("sandboxManager");

  return sandboxManager.getSandboxService(serviceName);
}

module.exports = getService;
