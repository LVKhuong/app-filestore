const Ajv = require('ajv');
const ajvFormat = require('ajv-formats');

const ajv = new Ajv({ allErrors: true });

ajvFormat(ajv);

const tagValidate = (args) => {
  const schema = {
    type: 'object',
    properties: {
      name: {
        type: 'string',
        minLength: 3,
      },
    },
    required: ['name'],
  }

  return ajv.compile(schema);
}

module.exports = tagValidate;
