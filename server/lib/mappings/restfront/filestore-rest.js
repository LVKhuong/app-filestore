"use strict";

module.exports = [
  {
    path: ['/files'],
    method: "GET",
    serviceName: "application/filestoreService",
    methodName: "getFiles",
    input: {
      transform: function transform(req) {
        return {
          _start: req.query._start,
          _end: (req.query._end - 1),
          _sort: req.query._sort,
          _order: req.query._order,
          tags: req.query.tags,
          createdAt_to: req.query.createdAt_to,
          createdAt_from: req.query.createdAt_from,
          downloadedAt_from: req.query.downloadedAt_from,
          downloadedAt_to: req.query.downloadedAt_to,
          q: req.query.q,
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            "X-Total-Count": response.total
          },
          body: response.data
        };
      }
    }
  }, 

  {
    path: ['/files/:id'],
    method: "GET",
    serviceName: "application/filestoreService",
    methodName: "getFile",
    input: {
      transform: function transform(req) {
        return {
          id: req.params.id
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            "X-Total-Count": 1,
          },
          body: response.data
        };
      }
    }
  }, 

  {
    path: ['/files/:id'],
    method: "PUT",
    serviceName: "application/filestoreService",
    methodName: "addTag",
    input: {
      transform: function transform(req) {
        return {
          tags: req.body.tags,
          id: req.params.id
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            // "X-Total-Count": response.total
          },
          body: response.data
        };
      }
    }
  }, 

  {
    path: '/files/:id',
    method: "DELETE",
    serviceName: "application/filestoreService",
    methodName: "deleteFileById",
    input: {
      transform: function transform(req) {
        return {
          id: req.params.id,
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {

          },
          body: response
        };
      }
    }
  }, 

];