"use strict";

const mongoose = require('app-datastore').require('mongoose');
const Schema = mongoose.Schema;

module.exports = {
  name: "TagModel",
  descriptor: {
    name: { type: String },
    deleted: { type: Boolean, default: false },
  },
  options: {
    collection: "tags",
    timestamps: true,
  },
};