"use strict";

const FileStoreModel = require("./models/filestore-model");
const TagModel = require("./models/tag-model");

const models = [
  FileStoreModel,
  TagModel,
];

module.exports = models;
